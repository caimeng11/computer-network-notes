### 交换机的基础配置与管理

+ 用户模式：Switch>
+ enable：从用户模式进入特权模式   Switch#
+ conf t：从特权模式进入全局配置模式   Switch(config)#
+ int f0/1：进入端口模式
+ exit：返回上级模式
+ ？：帮助命令
+ [tab]键：自动补全命令 例：en[tab] > enable

```java
出现：Translating "conft"...domain server (255.255.255.255)
// 解决方法
    1.在Switch(config)#:no ip domain-lookup    //关闭自动域名解析
    2.键盘ctrl+shift+6 中断自动域名解析
    3.等它好
```
